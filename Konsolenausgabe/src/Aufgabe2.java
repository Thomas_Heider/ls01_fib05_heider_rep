
public class Aufgabe2 {

	public static void main(String[] args) {
		System.out.println("Aufgabe 2: \n");
		
		System.out.printf("%-5s", "0!");
		System.out.printf("=");
		System.out.printf("%-19s", "");
		System.out.printf("=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s", "1!");
		System.out.printf("=");
		System.out.printf("%-19s", " 1");
		System.out.printf("=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s", "2!");
		System.out.printf("=");
		System.out.printf("%-19s", " 1 * 2");
		System.out.printf("=");
		System.out.printf("%4s\n", "2");
		
		System.out.printf("%-5s", "3!");
		System.out.printf("=");
		System.out.printf("%-19s", " 1 * 2 * 3");
		System.out.printf("=");
		System.out.printf("%4s\n", "6");
		
		System.out.printf("%-5s", "4!");
		System.out.printf("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4");
		System.out.printf("=");
		System.out.printf("%4s\n", "24");
		
		System.out.printf("%-5s", "5!");
		System.out.printf("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4 * 5");
		System.out.printf("=");
		System.out.printf("%4s\n", "120");
			
		}
	}


