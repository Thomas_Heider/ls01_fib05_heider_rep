
public class Aufgabe2Alt {

	public static void main(String[] args) {
		System.out.println("Aufgabe 2 alternative L�sung: \n");
		
		int maxvalue = 8;
		maxvalue++;
		int length = (maxvalue * 4) - 5;
		String converterstring = "%-" + length + "s";
		
		for(int i = 0; i < maxvalue; i++) {
			System.out.printf("%-5s", i + "!");
			System.out.printf("=");
			
			String m = "";
			
			for(int j = 1; j <= i; j++) {
				m = m + " " + j + " ";
				if(j < i)
					m = m + "*";
			}
			
			System.out.printf(converterstring, m);
			System.out.printf("=");
			System.out.printf("%4d\n", facul(i));
		}
	}
		
	private static int facul(int z) {
		if(z <= 1)
			return 1;
		return z * facul(z -1);
	}
}
