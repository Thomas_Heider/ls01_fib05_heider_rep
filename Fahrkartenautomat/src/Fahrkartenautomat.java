import java.util.Scanner;

//Erstellt von Thomas Heider und Nassim Preusse
class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		while (true) {
			System.out.println("-------------------------------------------------\n");
			int auswahl;
			double gesamtbetrag = 0;
			do {
				gesamtbetrag += fahrkartenbestellungErfassen(tastatur);
				boolean firstRun = true;
				do {
					if (!firstRun) {
						System.out.println("Ung�ltige Eingabe, bitte nur mit \"1\" oder \"2\" antworten");
					}
					System.out.println("Weitere Fahrkarten kaufen?(ja = 1/nein = 2)");
					auswahl = tastatur.nextInt();
					firstRun = false;
				} while (!(auswahl == 1 || auswahl == 2));
			} while (auswahl == 1);
			double r�ckgeld = fahrkartenBezahlen(tastatur, gesamtbetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(r�ckgeld);
		}
	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		final double PREIS_EINZELFAHRSCHEIN = 2.9;
		final double PREIS_TAGESKARTE = 8.6;
		final double PREIS_KLEINGRUPPE = 23.5;
		boolean firstRun = true;
		int auswahlFahrkartenart;

		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
		System.out.printf("Einzelfahrschein Regeltarif AB [%.2f EUR] (1)\n", PREIS_EINZELFAHRSCHEIN);
		System.out.printf("Tageskarte  Regeltarif AB [%.2f EUR] (2)\n", PREIS_TAGESKARTE);
		System.out.printf("Kleingruppen-Tageskarte Regeltarif AB [%.2f EUR] (3)\n", PREIS_KLEINGRUPPE);

		do {
			if (!firstRun) {
				System.out.println("Ung�ltige Eingabe! Ihre Wahl muss zwischen 1 und 3 liegen.");
			}
			System.out.print("Ihre Wahl: ");
			auswahlFahrkartenart = tastatur.nextInt();
			firstRun = false;
		} while (!((auswahlFahrkartenart > 0) && (auswahlFahrkartenart < 4)));

		double zuZahlenderBetrag;
		switch (auswahlFahrkartenart) {
		case 1:
			zuZahlenderBetrag = PREIS_EINZELFAHRSCHEIN;
			break;
		case 2:
			zuZahlenderBetrag = PREIS_TAGESKARTE;
			break;
		case 3:
			zuZahlenderBetrag = PREIS_KLEINGRUPPE;
			break;
		default:
			zuZahlenderBetrag = 0;
		}

		zuZahlenderBetrag *= anzahlFahrkarten(tastatur);
		return zuZahlenderBetrag;
	}

	public static int anzahlFahrkarten(Scanner tastatur) {

		int anzahlFahrkarten;
		boolean firstRun = true;

		do {
			if (!firstRun) {
				System.out.println("Ung�ltige Eingabe! Der Wert muss zwischen 1 und 10 liegen.");
			}
			System.out.print("Anzahl der Fahrkarten: ");
			anzahlFahrkarten = tastatur.nextInt();
			firstRun = false;
		} while (!((anzahlFahrkarten > 0) && (anzahlFahrkarten < 11)));

		return anzahlFahrkarten;
	}

	public static double fahrkartenBezahlen(Scanner tastatur, double gesamtbetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < gesamtbetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (gesamtbetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		double r�ckgabebetrag = eingezahlterGesamtbetrag - gesamtbetrag;
		return r�ckgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double r�ckgabebetrag) {
		r�ckgabebetrag += 0.00000000001; // Korrektur der Ausgabe, damit keine 5 ct fehlen
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.\n");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
